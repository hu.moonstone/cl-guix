(define-module (cl-guix packages cl-package)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system asdf)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (ice-9 match))

(define-public alexandria
  (let ((revision "1")
        (commit "fab0e59ce8f694cd8f790262aadca74403d8b973"))
    (package
      (name "alexandria")
      (version (git-version "1.0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.common-lisp.net/alexandria/alexandria.git")
               (commit commit)))
         (sha256
          (base32
           "0v5gmdcws2qaqrv16y1acsrksx956yb0rkk743awf259qa8f0bv3"))
         (file-name (git-file-name name version))))
      (build-system asdf-build-system/source)
      (synopsis "Collection of portable utilities for Common Lisp")
      (description
       "Alexandria is a collection of portable utilities.  It does not contain
conceptual extensions to Common Lisp.  It is conservative in scope, and
portable between implementations.")
      (home-page "https://common-lisp.net/project/alexandria/")
      (license license:public-domain))))

;; (define-public trivial-features
;;   (let ((revision "1")
;;         (commit "9d1d6591713c5190af63461b9bdcb3ec091cbd3a"))
;;     (package
;;      (name "trivial-features")
;;      (version (git-version "1.0.0" revision commit))
;;      (source
;;       (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/trivial-features/trivial-features.git")
;;              (commit commit)))
;;        (file-name (git-file-name name version))
;;        (sha256
;;         (base32 "11yl3h67dbnapib2jwinhprp7xqd7i2k4spw7y7cz0mipgwyvir1"))))
;;      (build-system asdf-build-system/source)
;;      (synopsis "trivial-features")
;;      (description "")
;;      (home-page "https://github.com/trivial-features/trivial-features")
;;      (license license:expat))))

;; (define-public trivial-channels
;;   (let ((revision "1")
;;         (commit "e2370118d8983ba69c0360a7695f8f2e2fd6a8a6"))
;;     (package
;;      (name "trivial-channels")
;;      (version (git-version "1.0.0" revision commit))
;;      (source
;;       (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/rpav/trivial-channels.git")
;;              (commit commit)))
;;        (file-name (git-file-name name version))
;;        (sha256
;;         (base32 "04wnxcgk40x8p0gxnz9arv1a5wasdqrdxa8c4p5v7r2mycfps6jj"))))
;;      (build-system asdf-build-system/source)
;;      (synopsis "trivial-channels")
;;      (description "")
;;      (home-page "https://github.com/rpav/trivial-channels")
;;      (license license:expat))))

;; (define-public cl-autowrap
;;   (let ((revision "1")
;;         (commit "2b12192ef136ab527881d2a4ca5ef098e3254282"))
;;     (package
;;      (name "cl-autowrap")
;;      (version (git-version "1.0.0" revision commit))
;;      (source
;;       (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/rpav/cl-autowrap.git")
;;              (commit commit)))
;;        (file-name (git-file-name name version))
;;        (sha256
;;         (base32 "03a9w00m5bwfx2zzcciz02zpg0cs3pwmbmsrwj0fj8vsq5wr6smq"))))
;;      (build-system asdf-build-system/source)
;;      (synopsis "cl-autowrap")
;;      (description "c2ffi-based wrapper generator for Common Lisp")
;;      (home-page "https://github.com/rpav/cl-autowrap")
;;      (license license:expat))))

;; (define-public cffi
;;   (let ((revision "1")
;;         (commit "31463855a1fffa5f45b6f38dc35dc53ff46d060c"))
;;     (package
;;      (name "cffi")
;;      (version (git-version "1.0.0" revision commit))
;;      (source
;;       (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/cffi/cffi.git")
;;              (commit commit)))
;;        (file-name (git-file-name name version))
;;        (sha256
;;         (base32 "0vd9vp7z5c3ycfnvy98dp0r41vgp80vc05n3js2z5flf7klhh4yr"))))
;;      (inputs `(("alexandria" ,alexandria)
;;                ("babel" ,babel)))
;;      (build-system asdf-build-system/source)
;;      (synopsis "cffi")
;;      (description "")
;;      (home-page "https://github.com/cffi/cffi/")
;;      (license license:expat))))

;; (define-public trivial-garbage
;;   (let ((revision "1")
;;         (commit "dbc8e35acb0176b9a14fdc1027f5ebea93435a84"))
;;     (package
;;      (name "trivial-garbage")
;;      (version (git-version "1.0.0" revision commit))
;;      (source
;;       (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/trivial-garbage/trivial-garbage.git")
;;              (commit commit)))
;;        (file-name (git-file-name name version))
;;        (sha256
;;         (base32 "01dr8gxzp19nkn1syjzpw8bm6mhfwwl47djzcxiikzfyvjw9mgpb"))))
;;      (build-system asdf-build-system/source)
;;      (synopsis "trivial-garbage")
;;      (description "")
;;      (home-page "https://github.com/trivial-garbage/trivial-garbage/")
;;      (license license:expat))))

;; (define-public cl-gettext
;;   (let ((revision "1")
;;         (commit "85d5dcea00e7448e10ac4f3ee0168bc691135fe9"))
;;     (package
;;      (name "cl-gettext")
;;      (version (git-version "1.0.0" revision commit))
;;      (source
;;       (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/krzysz00/cl-gettext.git")
;;              (commit commit)))
;;        (file-name (git-file-name name version))
;;        (sha256
;;         (base32 "0322pjl9s17zm6i3if8r78gks1fm9xp2w6hjw63793r5jahq3bhn"))))
;;      (inputs `(("cffi" , cl-cffi)))
;;      (build-system asdf-build-system/source)
;;      (synopsis "cl-gettext")
;;      (description "")
;;      (home-page "https://github.com/krzysz00/cl-gettext")
;;      (license license:lgpl2))))

;; (define-public babel
;;   (let ((revision "1")
;;         (commit "aeed2d1b76358db48e6b70a64399c05678a6b9ea"))
;;     (package
;;      (name "babel")
;;      (version (git-version "1.0.0" revision commit))
;;      (source
;;       (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/cl-babel/babel.git")
;;              (commit commit)))
;;        (file-name (git-file-name name version))
;;        (sha256
;;         (base32 "0lkvv4xdpv4cv1y2bqillmabx8sdb2y4l6pbinq6mjh33w2brpvb"))))
;;      (inputs `(("alexandria" ,alexandria)
;;                ("trivial-features" ,trivial-features)))
;;      (build-system asdf-build-system/source)
;;      (synopsis "babel")
;;      (description "")
;;      (home-page "https://github.com/cl-babel/babel/")
;;      (license license:expat))))


;; (define-public defpackage-plus
;;   (let ((revision "1")
;;         (commit "5492e27e0bdb7b75fa5177ea4388519dc7a75f11"))
;;     (package
;;      (name "defpackage-plus")
;;      (version (git-version "1.0.0" revision commit))
;;      (source
;;       (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/rpav/defpackage-plus/defpackage-plus.git")
;;              (commit commit)))
;;        (file-name (git-file-name name version))
;;        (sha256
;;         (base32 "0lzljvf343xb6mlh6lni2i27hpm5qd376522mk6hr2pa20vd6rdq"))))
;;      (inputs `(("alexandria" ,alexandria)))
;;      (build-system asdf-build-system/source)
;;      (synopsis "defpackage-plus")
;;      (description "")
;;      (home-page "https://github.com/rpav/defpackage-plus/")
;;      (license license:expat))))


(define-public cl-sdl2
  (let ((revision "1")
        (commit "1588954ee4abc37b01a7e2e17a76e84fd4da8c77"))
    (package
     (name "cl-sdl2")
     (version (git-version "1.0.0" revision commit))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/lispgames/cl-sdl2.git")
             (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "15x5d139qj3zba2qx3r43s1sh2bqgm4zcwd07kx2l23q2j89v509"))))
     (inputs `(("alexandria" ,alexandria)))
     (build-system asdf-build-system/source)
     (synopsis "cl-sdl2")
     (description "")
     (home-page "https://github.com/lispgames/cl-sdl2")
     (license license:expat))))

;; (define-public cl-sdl2-image
;;   (let ((revision "1")
;;         (commit "9c05c806286b66a5d9861ef829cfe68c4f3da077"))
;;     (package
;;      (name "cl-sdl2-image")
;;      (version (git-version "1.0.0" revision commit))
;;      (source
;;       (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/lispgames/cl-sdl2-image.git")
;;              (commit commit)))
;;        (file-name (git-file-name name version))
;;        (sha256
;;         (base32 "1nr7mdl125q32m15m8rdlza5kwi7m0birh1cq846pyy6zl1sjms7"))))
;;      (inputs `(("alexandria" ,alexandria)
;;                ("cl-autowrap" ,cl-autowrap)
;;                ("defpackage-plus", defpackage-plus)
;;                ("cl-sdl2", cl-sdl2)))
;;      (build-system asdf-build-system/source)
;;      (synopsis "cl-sdl2-image")
;;      (description "")
;;      (home-page "https://github.com/lispgames/cl-sdl2-image")
;;      (license license:expat))))


;; (define-public cl-sdl2-ttf
;;   (let ((revision "1")
;;         (commit "ff2821a2787cb527ca643a0dbca6ab2171ce141d"))
;;     (package
;;      (name "cl-sdl2-ttf")
;;      (version (git-version "1.0.0" revision commit))
;;      (source
;;       (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/Failproofshark/cl-sdl2-ttf.git")
;;              (commit commit)))
;;        (file-name (git-file-name name version))
;;        (sha256
;;         (base32 "1qj3s83f36ll7xmkaixrrpxscqdfhipzqjvhmav18pcgwwlqydr4"))))
;;      (inputs `(("alexandria" ,alexandria)
;;                ("cl-autowrap" ,cl-autowrap)
;;                ("defpackage-plus", defpackage-plus)
;;                ("cffi", cffi)
;;                ("trivial-garbage", trivial-garbage)
;;                ("cl-sdl2", cl-sdl2)))
;;      (build-system asdf-build-system/source)
;;      (synopsis "cl-sdl2-image")
;;      (description "")
;;      (home-page "https://github.com/Failproofshark/cl-sdl2-image")
;;      (license license:expat))))
